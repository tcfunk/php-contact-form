<?php

use PHPUnit\Framework\TestCase;
use \models\Contact;

/**
 * @covers \models\ Contact
 */
final class ContactTest extends TestCase
{
    // All fields present and valid
    public function testCanBeCreatedFromValidInput()
    {
        $input = [
            'full_name' => 'Test Person',
            'email' => 'test@pers.on',
            'phone' => '123-123-1234',
            'message' => 'Test',
        ];

        $this->assertInstanceOf(
            Contact::class,
            new Contact($input)
        );
    }

    // No phone number provided, all other fields present and valid
    public function testCanBeCreatedWithoutPhone()
    {
        $input = [
            'full_name' => 'Test Person',
            'email' => 'test@pers.on',
            'message' => 'Test',
        ];

        $this->assertInstanceOf(
            Contact::class,
            new Contact($input)
        );
    }

    // Reject missing email address
    public function testEmailAddressIsRequired()
    {
        $input = [
            'full_name' => 'Test Person',
            'message' => 'Test',
        ];

        $contact = new Contact($input);
        $contact->validate();

        $this->assertArrayHasKey('email', $contact->errors);
    }

    // Reject invalid email address
    public function testEmailAddressIsInvalid()
    {
        $input = [
            'full_name' => 'Test Person',
            'email' => 'test@person',
            'message' => 'Test',
        ];

        $contact = new Contact($input);
        $contact->validate();

        $this->assertArrayHasKey('email', $contact->errors);
    }

    // Reject missing full name
    public function testFullNameIsRequired()
    {
        $input = [
            'email' => 'test@person',
            'message' => 'Test',
        ];

        $contact = new Contact($input);
        $contact->validate();

        $this->assertArrayHasKey('full_name', $contact->errors);
    }

    // Reject missing message
    public function testMessageIsRequired()
    {
        $input = [
            'full_name' => 'Test Person',
            'email' => 'test@person',
        ];

        $contact = new Contact($input);
        $contact->validate();

        $this->assertArrayHasKey('message', $contact->errors);
    }

    public function testToArray()
    {
        $input = [
            'full_name' => 'Test',
            'email' => 'test@test.com',
            'message' => 'Message',
        ];
        $contact = new Contact($input);

        $this->assertEquals($contact->toArray(), $input);
    }

    public function testCantUpdateANewInstance()
    {
        $input = [
            'full_name' => 'Test',
            'email' => 'test@test.com',
            'message' => 'Message',
        ];

        $this->expectException(InvalidArgumentException::class);

        (new Contact($input))->update();
    }

}
