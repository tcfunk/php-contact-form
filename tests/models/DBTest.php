<?php

use PHPUnit\Framework\TestCase;
use \db\DB;

/**
 * @covers \db\DB
 */
final class DBTest extends TestCase
{
    public function testGetConnection()
    {
        $this->assertInstanceOf(
            \Slim\PDO\Database::class,
            DB::getConnection()
        );
    }
}