<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/bootstrap.php';


header('Content-Type: application/json');

$data = json_decode(file_get_contents('php://input'));
$contact = new \models\Contact($data);

// If save is successful, send email
if ($contact->save()) {
    $to = 'guy-smiley@example.com';
    $subject = 'Contact Request';
    $message = $contact->formatEmailMessage();
    $headers = 'From: noreply@example.com' . "\r\n" . 'Reply-To: '.$contact->email."\r\n";
    mail($to, $subject, $message, $headers);

    $response = '1';
}

// Otherwise, send back errors
else {
    http_response_code(400);
    $response = [
        'errors' => $contact->errors,
    ];
}

echo json_encode($response);
