"use strict";

window.onload = function() {

    new Vue({
        el: '#contact',

        data: {
            // Message data
            contact: {
                full_name: '',
                email: '',
                phone: '',
                message: '',
            },

            // Start empty, expects the format field_name: "Error string"
            errors: {},

            // Flag to show success message at the end
            messageSent: false
        },

        methods: {
            /**
             * Checks if this.errors contains any errors for
             * the given field.
             */
            hasError: function (field_name) {
                return this.errors.hasOwnProperty(field_name);
            },

            /**
             * Get error message for given field. If no error is defined,
             * return empty string.
             */
            errorMessage: function(field_name) {
                if (this.errors.hasOwnProperty(field_name)) {
                    return this.errors[field_name];
                }

                return '';
            },

            /**
             * Send request to server for validation/processing.
             * 
             * No client-side validation, rely on server to send back
             * error messages.
             */
            submit: function() {
                axios({
                    method: 'post',
                    url: '/contact',
                    data: this.contact,
                })
                .then((response) => {
                    this.messageSent = true;
                    this.errors = {};
                })
                .catch((error) => {
                    this.sending = false;
                    this.messageSent = false;
                    this.errors = error.response.data.errors;
               });
            }
        }

    });

};
