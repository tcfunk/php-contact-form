<?php
require_once 'vendor/autoload.php';

define('APP_ROOT', realpath(dirname(__FILE__)));

spl_autoload_register(function($class)
{
    $path = str_replace('\\', '/', $class);
    if (file_exists(APP_ROOT . "/src/{$path}.php")) {
        include_once APP_ROOT . "/src/{$path}.php";
    }
});
