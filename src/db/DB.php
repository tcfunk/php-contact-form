<?php

namespace db;

use \Slim\PDO\Database;

/**
 * DB static class.
 *
 * Thin wrapper around Slim\PDO\Database.
 */
class DB
{
    /**
     * Hold single instance of Slim\PDO\Database
     */
    private static $db;

    /**
     * Retrieve database connection. Create a new one if one
     * does not already exist.
     *
     * @throws Exception
     * @return DB
     */
    public static function getConnection()
    {
        if (!isset(self::$db)) {
            $config_file = APP_ROOT . '/config/db.ini';
            if (!file_exists($config_file)) {
                $example_file = APP_ROOT . '/config/db.ini.example';
                throw new Exception("Database config file {$config_file} does not exist. See {$example_file}");
            }

            $config = parse_ini_file($config_file);
            self::$db = new Database(
                self::createConnectionString($config),
                $config['username'],
                $config['password']);
        }

        return self::$db;
    }

    /**
     * Creates connection string based on provided config
     *
     * @param array $config
     * @return string
     */
    private static function createConnectionString($config)
    {
        return "{$config['driver']}:host={$config['host']};dbname={$config['database']}";
    }

}