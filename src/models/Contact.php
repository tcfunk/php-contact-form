<?php

namespace models;

use \InvalidArgumentException;
use \db\DB;

/**
 * Represents a Contact Message from the contact form on the front page.
 */
class Contact
{
    // Hold values for $fields
    protected $id;
    protected $full_name;
    protected $email;
    protected $phone;
    protected $message;

    // fields available for __set
    protected $fields = [
        'full_name',
        'email',
        'phone',
        'message',
    ];

    // Holds current error messages
    protected $errors;

    // fields to check for presence during validation
    protected $required_fields = [
        'full_name' => 'Full Name is required',
        'email' => 'Email address is required',
        'message' => 'Message is required',
    ];

    public function __construct($fields)
    {
        // Map provided fields to our own fields
        array_walk($fields, function($value, $field) {
            $this->__set($field, $value);
        });
    }

    /**
     * Setter function for full_name
     *
     * @param string $value
     * @return boolean
     */
    public function setFullname(string $value)
    {
        $this->full_name = $value;
        return true;
    }

    /**
     * Setter function for email
     *
     * @param string $value
     * @return boolean
     */
    public function setEmail(string $value)
    {
        $this->email = $value;
        return true;
    }

    /**
     * Setter function for message
     *
     * @param string $value
     * @return boolean
     */
    public function setMessage(string $value)
    {
        $this->message = $value;
        return true;
    }

    /**
     * Saves new model to the DB. Will forward to update if necessary.
     *
     * @return int|boolean
     */
    public function save()
    {
        // If instance has an id already, then we want to update
        // instead of insert
        if (!empty($this->id)) {
            return $this->update();
        }

        // Validate fields, return false if invalid
        if (!$this->validate()) {
            return false;
        }

        // Get database connection and prepare insert statement
        $db = DB::getConnection();
        $data = $this->toArray();
        $insert = $db->insert(array_keys($data))
                     ->into('contacts')
                     ->values(array_values($data));

        // Execute insert statement and return result
        return $insert->execute();
    }

    /**
     * Saves existing model to the database.
     *
     * @throws InvalidArgumentException if called on an object with no $id
     * @return int|boolean
     */
    public function update()
    {
        // While calling save on an existing instance probably makes sense,
        // calling update on a new one does not.
        if (empty($this->id)) {
            throw new InvalidArgumentException('Called update on non-existing row. Use Contact::save instead.');
        }

        // Get database connection and prepare update statement
        $db = DB::getConnection();
        $update = $db->update($this->toArray())
                     ->table(self::DB_TABLE)
                     ->where('id', '=', $this->id);

        // Execute update statement and return result
        return $update->execute();
    }

    /**
     * Validate fields individually. Store messages in $errors
     * if there are any.
     *
     * @return boolean
     */
    public function validate()
    {
        $errors = [];

        // Check presence of required fields
        foreach ($this->required_fields as $field => $message) {
            if (!isset($this->$field) || empty($this->$field)) {
                $errors[$field] = $message;
            }
        }

        // Check that email is properly formatted
        if (!array_key_exists('email', $errors)) {
            if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
                $errors['email'] = 'Email is invalid';
            }
        }

        // Assign errors, and return valid status
        $this->errors = $errors;
        return empty($errors);
    }

    /**
     * Getter function for model properties
     *
     * @param string $field
     * @return mixed
     */
    public function __get(string $field)
    {
        // if we have the specified field, return it's value 
        if (in_array($field, array_keys($this->fields))) {
            return $this->$field;
        }

        // Special case for errors
        if ($field === 'errors') {
            return $this->errors;
        }

        // otherwise return null
        return null;
    }

    /**
     * Setter function for model propertie
     *
     * @param string $field
     * @param mixed $value
     * @return boolean
     */
    public function __set(string $field, $value)
    {
        // echo PHP_EOL."called __set($field, $value)";

        // check that specified field exists
        if (!in_array($field, $this->fields)) {
           return false;
        }

        // if we have a setter for the given field, use that
        $method = 'set'.ucfirst($field);
        if (is_callable(array($this, $method))) {
            $this->$method($value);
        }

        // Special case for full_name
        else if ($field === 'full_name') {
            $this->setFullname($value);
        }

        // If not, just set the field to the given value
        else {
            $this->$field = $value;
        }
    }

    /**
     * Converts contact instance into array of field => value pairs
     *
     * @return array
     */
    public function toArray()
    {
        $values = [];
        foreach ($this->fields as $field) {
            if (isset($this->$field) && !empty($this->$field)) {
                $values[$field] = $this->$field;
            }
        }
        return $values;
    }

    public function formatEmailMessage()
    {
        $parts = [];
        $separator = "\r\n";

        $parts[]= 'From: '.$this->full_name.'<'.$this->email.'>';

        if (isset($this->phone) && !empty($this->phone)) {
            $parts[]= 'Phone: '.$this->phone;
        }

        $parts[] = $separator.wordwrap($this->message, 70, $separator);

        return implode($separator, $parts);
    }

}
